package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		if (args.length < 3)
			try {
				throw new Exception("args length is lower 3");
			} catch (Exception e) {
				e.printStackTrace();
			}

		String forwardReadFile = args[0]; // maybe a piped stream
		String reverseReadFile = args[1]; // maybe a piped stream
		String barcodeFileName = args[2];
		String outputFolder;
		if (args.length > 3 && !args[3].equals("-cut"))
			outputFolder = args[3]; // important if input is <(gunzip -c ...)
		else
			outputFolder = new File(forwardReadFile).getParent();

		int numberOfCharsToCut = 0;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-cut") && (i + 1) < args.length)
				numberOfCharsToCut = Integer.parseInt(args[i + 1]);
		}

		String[] barcodesArr = readInBarcodes(barcodeFileName);

		// ClassifyReadsSingleThread.matchSeq(barcodesArr, forwardReadFile,
		// reverseReadFile, 0, outputFolder, numberOfCharsToCut);
		ClassifyReads.matchSeq(barcodesArr, forwardReadFile, reverseReadFile, 0, outputFolder, numberOfCharsToCut);
	}

	static String[] readInBarcodes(String barcodeFileName) {
		List<String> barcodeList = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(barcodeFileName));
			String line = br.readLine();
			while (line != null) {
				barcodeList.add(line.split(" ")[0]);
				line = br.readLine();
			}
			br.close();
			if (barcodeList.isEmpty())
				throw new Exception("Barcode file (" + barcodeFileName + ") contains no barcodes!");
		} catch (FileNotFoundException e) {
			System.out.println("Barcode file (" + barcodeFileName + ") not found!");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return barcodeList.toArray(new String[barcodeList.size()]);
	}
}