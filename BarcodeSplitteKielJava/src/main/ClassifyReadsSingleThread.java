package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;

public class ClassifyReadsSingleThread {
	public static void matchSeq(String[] barcodesArr, String forwardReadFile, String reverseReadFile,
			int maxHammingDist, String outputFolder, int numberOfCharsToCut) {

		PerformanceMeasure timeMeasure = new PerformanceMeasure();

		Consumer consumer = new Consumer(null, barcodesArr, outputFolder, maxHammingDist, timeMeasure);
		Producer producer = new Producer(forwardReadFile, reverseReadFile, null, timeMeasure, numberOfCharsToCut);

		int barcodeLength = barcodesArr[0].length();
		int blockSize = 4;
		long lineCounter = 0;
		long forwardBytesRead = 0, reverseBytesRead = 0;
		long forwardBytesWritten = 0, reverseBytesWritten = 0;
		HashMap<String, Long> barcodeCounter = new HashMap<String, Long>();
		BufferedWriter[][] bufferedWriterArr = consumer.getBufferedWriterArr(outputFolder, barcodeCounter);

		try {
			BufferedReader forwardFileReader = producer.getBufferedReaderFromFile(forwardReadFile);
			BufferedReader reverseFileReader = producer.getBufferedReaderFromFile(reverseReadFile);
			String[] forwardBlock = new String[4];
			String[] reverseBlock = new String[4];
			int blockPtr = 0;
			final int newLineSize = System.getProperty("line.separator").length();

			while (forwardFileReader.ready()) {
				lineCounter++;
				// read block by block
				forwardBlock[blockPtr] = forwardFileReader.readLine();
				forwardBytesRead += forwardBlock[blockPtr].length();
				reverseBlock[blockPtr] = reverseFileReader.readLine();
				reverseBytesRead += reverseBlock[blockPtr].length();
				blockPtr = (blockPtr + 1) % blockSize;
				// block filled => write it
				if (blockPtr == 0) {
					// determine Barcode from second line in Barcode-Block
					String supposedBarcode = forwardBlock[1].substring(3, 3 + barcodeLength);
					int choosenBarcodePos = consumer.getBarcodeWithLowestHammingDist(supposedBarcode, maxHammingDist);

					// if the barcode is not unmatched
					if (choosenBarcodePos != -1) {
						forwardBytesWritten += Consumer.getSize(forwardBlock);
						// write forward read block
						consumer.writeStringArr(bufferedWriterArr[choosenBarcodePos][0], forwardBlock);

						reverseBytesWritten += Consumer.getSize(reverseBlock);
						// write reverse read block
						consumer.writeStringArr(bufferedWriterArr[choosenBarcodePos][1], reverseBlock);

						long counter = barcodeCounter.get(barcodesArr[choosenBarcodePos]);
						barcodeCounter.put(barcodesArr[choosenBarcodePos], counter + 1);
					}
				}
			}
			forwardFileReader.close();
			reverseFileReader.close();

			forwardBytesRead += lineCounter * newLineSize;
			reverseBytesRead += lineCounter * newLineSize;

			// transfer read bytes
			timeMeasure.setReadBytes(forwardBytesRead, reverseBytesRead);
			timeMeasure.setReadLines(lineCounter);

			// close all writers to get clean results
			consumer.closeBufferedWriterMap(bufferedWriterArr);

			forwardBytesWritten += lineCounter * newLineSize;
			reverseBytesWritten += lineCounter * newLineSize;

			for (String barcode : barcodeCounter.keySet()) {
				System.out.println(barcode + ": " + barcodeCounter.get(barcode));
			}

			// finish and print stats
			timeMeasure.setWrittenLines(lineCounter);
			timeMeasure.setWriteBytes(forwardBytesWritten, reverseBytesWritten);
			timeMeasure.end();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
