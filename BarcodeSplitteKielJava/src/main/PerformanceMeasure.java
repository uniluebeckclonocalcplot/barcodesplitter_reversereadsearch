package main;

public class PerformanceMeasure {
	long startTime;
	long estimatedTime;
	long forwardBytesRead = 0, reverseBytesRead = 0;
	long forwardBytesWritten = 0, reverseBytesWritten = 0;
	long lineCounterRead, lineCounterWritten;
	boolean compressedInput = false;

	public PerformanceMeasure() {
		startTime = System.nanoTime();
	}

	public void end() {
		System.out.println("Unmatched: " + ((lineCounterRead - lineCounterWritten) / 4));

		estimatedTime = System.nanoTime() - startTime;

		System.out.println("Runtime: " + (estimatedTime / 1000000000.0) + " sec");
		System.out.println(lineCounterRead + " lines read.");

		if (compressedInput)
			System.out.println("Input was gzipped! => These are not the disk read rates");

		System.out.println("Forward Read  " + toMiBStr(forwardBytesRead) + " => "
				+ toMiBPerSecStr(estimatedTime, forwardBytesRead));
		System.out.println("Reverse Read  " + toMiBStr(reverseBytesRead) + " => "
				+ toMiBPerSecStr(estimatedTime, reverseBytesRead));
		System.out.println("Read ges      " + toMiBStr(forwardBytesRead + reverseBytesRead) + " => "
				+ toMiBPerSecStr(estimatedTime, forwardBytesRead + reverseBytesRead));

		System.out.println(lineCounterWritten + " lines written.");
		System.out.println("Forward Write " + toMiBStr(forwardBytesWritten) + " => "
				+ toMiBPerSecStr(estimatedTime, forwardBytesWritten));
		System.out.println("Reverse Write " + toMiBStr(reverseBytesWritten) + " => "
				+ toMiBPerSecStr(estimatedTime, reverseBytesWritten));
		System.out.println("Write ges     " + toMiBStr(forwardBytesWritten + reverseBytesWritten) + " => "
				+ toMiBPerSecStr(estimatedTime, forwardBytesWritten + reverseBytesWritten));
	}

	public void setReadBytes(long forwardBytesRead, long reverseBytesRead) {
		this.forwardBytesRead = forwardBytesRead;
		this.reverseBytesRead = reverseBytesRead;
	}

	public void setReadLines(long lineCounterRead) {
		this.lineCounterRead = lineCounterRead;
	}

	public void setWrittenLines(long lineCounterWritten) {
		this.lineCounterWritten = lineCounterWritten;
	}

	public void setCompressedInputFlag() {
		compressedInput = true;
	}

	private String toMiBStr(long bytes) {
		return (bytes / (double) (1024 * 1024)) + " MiB";
	}

	private String toMiBPerSecStr(long estimatedTime2, long bytes) {
		return (bytes / (double) (1024 * 1024)) / (estimatedTime / 1000000000.0) + " MiB/s";
	}

	public void setWriteBytes(long forwardBytesWritten, long reverseBytesWritten) {
		this.forwardBytesWritten = forwardBytesWritten;
		this.reverseBytesWritten = reverseBytesWritten;
	}
}
