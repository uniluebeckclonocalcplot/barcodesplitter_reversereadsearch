package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.Semaphore;
import java.util.zip.GZIPInputStream;

public class ClassifyReads {
	public static void matchSeq(String[] barcodesArr, String forwardReadFile, String reverseReadFile,
			int maxHammingDist, String outputFolder, int numberOfCharsToCut) {

		PerformanceMeasure timeMeasure = new PerformanceMeasure();

		Data data = new Data();
		Producer producer = new Producer(forwardReadFile, reverseReadFile, data, timeMeasure, numberOfCharsToCut);
		Consumer consumer = new Consumer(data, barcodesArr, outputFolder, maxHammingDist, timeMeasure);

		//
		producer.start();
		consumer.start();
	}
}

class Data {
	int maxBlocks = 1000;
	String[][] forwardBlocks = new String[maxBlocks][];
	String[][] reverseBlocks = new String[maxBlocks][];
	int producerPos = 0;
	int consumerPos = 0;
	Semaphore SemaProducer = new Semaphore(maxBlocks, true);
	Semaphore SemaConsumer = new Semaphore(0, true);
	boolean allBlocksRead = false;
	static int blockSize = 4;

	public void setAllBlocksRead() {
		allBlocksRead = true;
		// if consumer hangs on semaphore
		SemaConsumer.release();
	}

	public void produce(String[] forwardBlock, String[] reverseBlock) {
		// blocks until the Consumer eat some blocks
		try {
			SemaProducer.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		forwardBlocks[producerPos] = Arrays.copyOf(forwardBlock, blockSize);
		reverseBlocks[producerPos] = Arrays.copyOf(reverseBlock, blockSize);
		producerPos = (producerPos + 1) % maxBlocks;
		SemaConsumer.release();
	}

	public String[][] consume() {
		// blocks until the Producer make some blocks
		try {
			SemaConsumer.acquire();

			// if all blocks are taken
			if (allBlocksRead && producerPos == consumerPos)
				return null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		String[][] ret = new String[2][];
		ret[0] = Arrays.copyOf(forwardBlocks[consumerPos], blockSize);
		ret[1] = Arrays.copyOf(reverseBlocks[consumerPos], blockSize);

		consumerPos = (consumerPos + 1) % maxBlocks;
		SemaProducer.release();

		return ret;
	}
}

class Producer extends Thread {
	PerformanceMeasure timeMeasure;
	String forwardReadFile;
	String reverseReadFile;
	Data data;
	static int blockSize = 4;
	long lineCounter = 0;
	long forwardBytesRead = 0, reverseBytesRead = 0;
	int numberOfCharsToCut;

	public Producer(String forwardReadFile, String reverseReadFile, Data data, PerformanceMeasure timeMeasure,
			int numberOfCharsToCut) {
		this.forwardReadFile = forwardReadFile;
		this.reverseReadFile = reverseReadFile;
		this.data = data;
		this.timeMeasure = timeMeasure;
		this.numberOfCharsToCut = numberOfCharsToCut;
	}

	public void run() {
		try {
			BufferedReader forwardFileReader = getBufferedReaderFromFile(forwardReadFile);
			BufferedReader reverseFileReader = getBufferedReaderFromFile(reverseReadFile);
			String[] forwardBlock = new String[4];
			String[] reverseBlock = new String[4];
			int blockPtr = 0;
			final int newLineSize = System.getProperty("line.separator").length();

			// char[] forwardBuf = new char[100000];
			// char[] reverseBuf = new char[100000];

			while (forwardFileReader.ready()) {
				lineCounter++;
				// read block by block
				forwardBlock[blockPtr] = forwardFileReader.readLine();
				forwardBytesRead += forwardBlock[blockPtr].length();
				reverseBlock[blockPtr] = reverseFileReader.readLine();
				reverseBytesRead += reverseBlock[blockPtr].length();

				if (numberOfCharsToCut > 0 && blockPtr == 1) {
					int length = forwardBlock[blockPtr].length() - numberOfCharsToCut;
					if (0 > length)
						forwardBlock[blockPtr] = forwardBlock[blockPtr].substring(0, length);

					length = reverseBlock[blockPtr].length() - numberOfCharsToCut;
					if (0 > length)
						reverseBlock[blockPtr] = reverseBlock[blockPtr].substring(0, length);
				}

				blockPtr = (blockPtr + 1) % blockSize;
				// block filled => write it
				if (blockPtr == 0) {
					data.produce(forwardBlock, reverseBlock);
				}

				// forwardBytesRead += forwardFileReader.read(forwardBuf);
				// reverseBytesRead += reverseFileReader.read(reverseBuf);

			}
			forwardFileReader.close();
			reverseFileReader.close();

			forwardBytesRead += lineCounter * newLineSize;
			reverseBytesRead += lineCounter * newLineSize;

			// transfer read bytes
			timeMeasure.setReadBytes(forwardBytesRead, reverseBytesRead);
			timeMeasure.setReadLines(lineCounter);

			// stop the consumer process
			data.setAllBlocksRead();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public BufferedReader getBufferedReaderFromFile(String filePath) {
		// handle device files e.g. gunzip -c
		if (!filePath.startsWith("/dev")) {
			try {
				FileInputStream fis = new FileInputStream(filePath);
				GZIPInputStream gis = new GZIPInputStream(fis);
				// If this line does not throw exception your file is GZip
				// It's reads the first two bytes, so if we have a device file
				// e.g. created by gunzip -c we cannot jump back to the start

				timeMeasure.setCompressedInputFlag();
				Reader decoder = new InputStreamReader(gis);
				return new BufferedReader(decoder);

			} catch (IOException e) {
				// Not in GZip Format
			}
		}
		try {
			return new BufferedReader(new FileReader(filePath));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		return null;
	}
}

class Consumer extends Thread {
	PerformanceMeasure timeMeasure;
	Data data;
	String[] barcodesArr;
	static int blockSize = 4;
	int barcodeLength;
	String outputFolder;
	int maxHammingDist;
	long lineCounter = 0;
	long forwardBytesWritten = 0, reverseBytesWritten = 0;
	HashMap<String, Long> barcodeCounter = new HashMap<String, Long>();
	HashMap<String, Integer> hammingMap;

	public Consumer(Data data, String[] barcodesArr, String outputFolder, int maxHammingDist,
			PerformanceMeasure timeMeasure) {
		this.data = data;
		this.barcodesArr = barcodesArr;
		this.barcodeLength = barcodesArr[0].length();
		this.outputFolder = outputFolder;
		this.maxHammingDist = maxHammingDist;
		if (maxHammingDist == 0) {
			hammingMap = new HashMap<String, Integer>();
			for (int i = 0; i < barcodesArr.length; i++)
				hammingMap.put(barcodesArr[i], i);
		}
		this.timeMeasure = timeMeasure;
	}

	public void run() {
		BufferedWriter[][] bufferedWriterArr = getBufferedWriterArr(outputFolder, barcodeCounter);
		String[][] forwardReverseBlock = data.consume();
		// null if all blocks were taken
		while (forwardReverseBlock != null) {

			// determine Barcode from second line in Barcode-Block
			String supposedBarcode = forwardReverseBlock[0][1].substring(3, 3 + barcodeLength);
			int choosenBarcodePos = getBarcodeWithLowestHammingDist(supposedBarcode, maxHammingDist);

			// if the barcode is not unmatched
			if (choosenBarcodePos != -1) {
				lineCounter += 4; // size of each block

				forwardBytesWritten += getSize(forwardReverseBlock[0]);
				// write forward read block
				writeStringArr(bufferedWriterArr[choosenBarcodePos][0], forwardReverseBlock[0]);

				reverseBytesWritten += getSize(forwardReverseBlock[1]);
				// write reverse read block
				writeStringArr(bufferedWriterArr[choosenBarcodePos][1], forwardReverseBlock[1]);

				long counter = barcodeCounter.get(barcodesArr[choosenBarcodePos]);
				barcodeCounter.put(barcodesArr[choosenBarcodePos], counter + 1);
			}

			forwardReverseBlock = data.consume();
		}
		// close all writers to get clean results
		closeBufferedWriterMap(bufferedWriterArr);

		final int newLineSize = System.getProperty("line.separator").length();
		forwardBytesWritten += lineCounter * newLineSize;
		reverseBytesWritten += lineCounter * newLineSize;

		for (String barcode : barcodeCounter.keySet()) {
			System.out.println(barcode + ": " + barcodeCounter.get(barcode));
		}
		// finish and print stats
		timeMeasure.setWrittenLines(lineCounter);
		timeMeasure.setWriteBytes(forwardBytesWritten, reverseBytesWritten);
		timeMeasure.end();
	}

	public void writeStringArr(BufferedWriter bw, String[] strArr) {
		for (int i = 0; i < strArr.length; i++) {
			try {
				bw.write(strArr[i]);
				bw.newLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public BufferedWriter[][] getBufferedWriterArr(String outputFolder, HashMap<String, Long> barcodeCounter) {
		BufferedWriter[][] bufferedWriterArr = new BufferedWriter[barcodesArr.length][2];
		for (int i = 0; i < barcodesArr.length; i++) {
			String barcode = barcodesArr[i];
			try {
				barcodeCounter.put(barcode, (long) 0);

				BufferedWriter forwardWriter = new BufferedWriter(
						new FileWriter(outputFolder + File.separator + barcode + ".fastq"));
				BufferedWriter reverseWriter = new BufferedWriter(
						new FileWriter(outputFolder + File.separator + barcode + "_rev.fastq"));
				bufferedWriterArr[i] = new BufferedWriter[] { forwardWriter, reverseWriter };
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bufferedWriterArr;
	}

	public void closeBufferedWriterMap(BufferedWriter[][] bufferedWriterArrArr) {
		for (BufferedWriter[] bufferedWriterArr : bufferedWriterArrArr) {
			for (BufferedWriter bw : bufferedWriterArr)
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	public int getBarcodeWithLowestHammingDist(String supposedBarcode, int maxHammingDist) {

		if (maxHammingDist == 0) {
			Integer value = hammingMap.get(supposedBarcode);
			if (value != null)
				return value;
			else
				return -1;
		}

		int bestApproxBarcode = -1;
		int bestDist = Integer.MAX_VALUE;
		int i = 0;
		for (; i < barcodesArr.length; i++) {
			int dist = getHammingDist(supposedBarcode, barcodesArr[i]);
			if (dist == 0)
				return i;
			else if (dist < bestDist)
				bestApproxBarcode = i;
		}
		if (bestDist > maxHammingDist)
			return -1;
		return bestApproxBarcode;
	}

	public static int getHammingDist(String a, String b) {
		int distance = 0;

		for (int i = 0; i < a.length(); i++) {
			if (a.charAt(i) != b.charAt(i)) {
				distance++;
			}
		}

		return distance;
	}

	public static long getSize(String arr[]) {
		long size = 0;
		for (int i = 0; i < arr.length; i++)
			size += arr[i].length();
		return size;
	}
}